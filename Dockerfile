FROM alpine:3.8

RUN apk update && \
    apk add bash && \
    apk add curl && \
    apk add lftp

RUN curl -s https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.1.0/common.sh > /common.sh

COPY pipe /

ENTRYPOINT ["/pipe.sh"]
